package com.example.intelli.hotel.IntelliHotel.repositories;

import com.example.intelli.hotel.IntelliHotel.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserRepo extends JpaRepository<ApplicationUser, Long> {
    public ApplicationUser findByUsername(String username);
}
