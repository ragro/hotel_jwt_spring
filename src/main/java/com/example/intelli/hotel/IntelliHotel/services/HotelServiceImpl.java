package com.example.intelli.hotel.IntelliHotel.services;

import com.example.intelli.hotel.IntelliHotel.model.Hotel;
import com.example.intelli.hotel.IntelliHotel.model.Review;
import com.example.intelli.hotel.IntelliHotel.repositories.HotelRepo;
import com.example.intelli.hotel.IntelliHotel.repositories.ReviewRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class HotelServiceImpl implements  HotelService {

    @Autowired
    HotelRepo hotelRepo;

    @Autowired
    ReviewRepo reviewRepo;

    @Override
    public List<Hotel> addHotel() {
        Hotel hotel1 = new Hotel();
        hotel1.setName("Taj");
        hotel1.setAddress("Mumbai");

        Review review1 = new Review();
        review1.setComment("Outstanding place to visit");
        review1.setRating(5);

        hotel1.setReviewList(Arrays.asList(review1));

        hotelRepo.save(hotel1);

        return hotelRepo.findAll();
    }

    @Override
    public String addNewHotel(Hotel hotel) {
//        Review review1 = new Review();
//        review1.setComment("Outstanding place to visit");
//        review1.setRating(5);
//
//        hotel.setReviewList(Arrays.asList(review1));
        hotelRepo.save(hotel);
        return "added";
    }

    @Override
    public List<Hotel> getHotels() {
        return hotelRepo.findAll();
    }

    @Override
    public void deleteHotel(Long hotel_id){
        System.out.println(hotel_id);
        hotelRepo.deleteById(hotel_id);
    }

    @Override
    public void addReview(Long hotel_id, Review review){
        Hotel hotel = hotelRepo.findById(hotel_id).get();
        List<Review> reviews = hotel.getReviewList();
        reviews.add(review);
        hotel.setReviewList(reviews);

        hotelRepo.save(hotel);

        Hotel hotel2 = hotelRepo.findById(hotel_id).get();
        System.out.println(hotel2);
    }

    @Override
    public void deleteReview(Long hotel_id, Long review_id) {
        Hotel hotel = hotelRepo.findById(hotel_id).get();
        List<Review> reviews = hotel.getReviewList();

        System.out.println(reviews);

        for(Review review : reviews){
            if(review.getReview_id() == review_id){
                reviews.remove(review);
                break;
            }
        }

        hotel.setReviewList(reviews);
        hotelRepo.save(hotel);

        List<Review> reviews2 = hotel.getReviewList();
        System.out.println(reviews2);
    }
}
