package com.example.intelli.hotel.IntelliHotel.controller;

import com.example.intelli.hotel.IntelliHotel.model.ApplicationUser;
import com.example.intelli.hotel.IntelliHotel.repositories.ApplicationUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    public ApplicationUserRepo applicationUserRepo;
    @Autowired
    public BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(ApplicationUserRepo applicationUserRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepo = applicationUserRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public void signUp(@RequestBody ApplicationUser user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepo.save(user);
    }
}
