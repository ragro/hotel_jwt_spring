package com.example.intelli.hotel.IntelliHotel.repositories;

import com.example.intelli.hotel.IntelliHotel.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepo extends JpaRepository<Review, Long> {
}
