package com.example.intelli.hotel.IntelliHotel.services;

import com.example.intelli.hotel.IntelliHotel.model.Hotel;
import com.example.intelli.hotel.IntelliHotel.model.Review;

import java.util.List;

public interface HotelService {

    public List<Hotel> addHotel();
    public List<Hotel> getHotels();
    public String addNewHotel(Hotel hotel);
    public void deleteHotel(Long hotel_id);
    public void addReview(Long hotel_id, Review review);
    public void deleteReview(Long hotel_id, Long review_id);
}
